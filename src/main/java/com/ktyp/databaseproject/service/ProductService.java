/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ktyp.databaseproject.service;

import com.ktyp.databaseproject.dao.ProductDao;
import com.ktyp.databaseproject.model.Product;
import java.util.List;

/**
 *
 * @author toey
 */
public class ProductService {

    public List<Product> getProducts() {
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" customer_id asc");
    }

    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }

}
